import json
import asyncio
import json
import logging
import websockets
import os

logging.basicConfig()

# If there is no configuration file sds.json, create one
if not os.path.isfile('sds.json'):
    with open('sds.json', 'w+') as f:
        json.dump({'allowed_events': ['test']}, f)

# Load the allowed events from the config file sds.json
with open('sds.json') as f:
    allowed_events = json.load(f)['allowed_events']

import glob
modules = glob.glob("ext/*.py")
for i in [ f for f in modules if os.path.isfile(f) and not f.endswith('__init__.py')]:
    import importlib.util
    spec = importlib.util.spec_from_file_location("useless.module", i)
    mod = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(mod)

import reg

ext_commands = reg.ext_commands

subscribers = {}


# Create an event class that has a type and a data attribute
class Event:
    def __init__(self, type, data):
        self.type = type
        self.data = data



async def broadcast_events():
    while not events_to_broadcast.empty():
        event = await events_to_broadcast.get()
        for subscriber in subscribers.get(event.type, []):
            await subscriber.send(event.data)



async def unregister(websocket):
    for subscriber_list in subscribers.values():
        subscriber_list.remove(websocket)

class Response:
    def __init__(self, error, data):
        self.error = error
        self.data = data

    def toJson(self):
        return json.dumps(self, default=lambda o: o.__dict__,)

async def counter(websocket, path):
    try:
        await websocket.send(Response(False, "HELLO").toJson())
        async for message in websocket:
            command = message.split()
            if not command:
                continue
            async def rl(l):
                if len(command) != l:
                    await websocket.send(Response(True, "Invalid command: expected {} arguments".format(l-1)).toJson())
                    return False
                return True
            if command[0] in ext_commands:
                ext_commands[command[0]](websocket, command, rl)
            elif command[0] == 'subscribe':
                if not await rl(2):
                    continue
                if command[1] not in allowed_events:
                    await websocket.send(Response(True, "Invalid event").toJson())
                    continue
                if subscribers.get(command[1], None) is None:
                    subscribers[command[1]] = []
                subscribers[command[1]].append(websocket)
                await websocket.send(Response(False, "Subscribed to " + command[1]).toJson())
            elif command[0] == 'unsubscribe':
                if not await rl(2):
                    continue
                if command[1] not in allowed_events:
                    await websocket.send(Response(True, "Invalid event").toJson())
                    continue
                if subscribers.get(command[1], None) is None:
                    pass
                else:
                    subscribers[command[1]].remove(websocket)
                await websocket.send(Response(False, "Unsubscribed from " + command[1]).toJson())
            elif command[0] == 'list':
                if not await rl(1):
                    continue
                await websocket.send(Response(False, allowed_events).toJson())
            else:
                await websocket.send(Response(True, "Invalid command").toJson())
    except:
        await websocket.send(Response(True, "Unknown Server Error").toJson())
        await unregister(websocket)

import aioconsole

DEBUG = True

async def loop():
    while True:
        if DEBUG:
            line = await aioconsole.ainput('e to add test event')
            print(subscribers)
            if line == 'e':
                await events_to_broadcast.put(Event('test', 'test event'))
            print(events_to_broadcast._queue)
        await broadcast_events()


aloop = asyncio.new_event_loop()
asyncio.set_event_loop(aloop)

start_server = websockets.serve(counter, "localhost", 6789)


events_to_broadcast = asyncio.Queue()

aloop.run_until_complete(start_server)
aloop.run_until_complete(loop())
